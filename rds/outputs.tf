output "rds_addresses" {
    value = "${aws_db_instance.WeatherApp_DB.*.address}"
}


output "rds_password" {
    value = "${random_string.rds_password.result}"
}